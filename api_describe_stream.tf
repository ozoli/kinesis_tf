resource "aws_api_gateway_resource" "stream" {
  count       = var.create_api_gateway
  rest_api_id = aws_api_gateway_rest_api.mod[0].id
  parent_id   = aws_api_gateway_resource.streams[0].id
  path_part   = "{stream-name}"
}

resource "aws_api_gateway_method" "describe_stream" {
  count            = var.create_api_gateway
  rest_api_id      = aws_api_gateway_rest_api.mod[0].id
  resource_id      = aws_api_gateway_resource.stream[0].id
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_integration" "describe_stream" {
  count                   = var.create_api_gateway
  rest_api_id             = aws_api_gateway_rest_api.mod[0].id
  resource_id             = aws_api_gateway_resource.stream[0].id
  http_method             = aws_api_gateway_method.describe_stream[0].http_method
  type                    = "AWS"
  integration_http_method = "POST"
  uri                     = "arn:aws:apigateway:${var.aws_region}:kinesis:action/DescribeStream"
  passthrough_behavior    = "WHEN_NO_TEMPLATES"
  credentials             = aws_iam_role.gateway_execution_role[0].arn

  request_parameters = {
    "integration.request.header.Content-Type" = "'application/x-amz-json-1.1'"
  }

  # Transforms the incoming XML request to JSON
  request_templates = {
    "application/json" = <<EOF
{
    "StreamName": "$input.params('stream-name')"
}
EOF

  }
}

resource "aws_api_gateway_method_response" "describe_stream_ok" {
  count       = var.create_api_gateway
  depends_on  = [aws_api_gateway_method.describe_stream]
  rest_api_id = aws_api_gateway_rest_api.mod[0].id
  resource_id = aws_api_gateway_resource.stream[0].id
  http_method = aws_api_gateway_method.describe_stream[0].http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "describe_stream_ok" {
  count       = var.create_api_gateway
  rest_api_id = aws_api_gateway_rest_api.mod[0].id
  resource_id = aws_api_gateway_resource.stream[0].id
  http_method = aws_api_gateway_method.describe_stream[0].http_method
  status_code = aws_api_gateway_method_response.describe_stream_ok[0].status_code

  # Passthrough the JSON response
  response_templates = {
    "application/json" = <<EOF

EOF

  }
}

